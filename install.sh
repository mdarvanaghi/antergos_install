#!/bin/bash

RUN_DIR=$PWD

print(){
    echo -e "\033[47;30m\033[47m$1\033[0m"
}

### PACKAGE MANAGER
print "Enabling color in pacman..."
sudo sed -i 's/#Color/Color/g' /etc/pacman.conf

print "Installing yay..."
sudo pacman -S yay --noconfirm

print "Updating system..."
yay --noconfirm

### .CONFIG
print "Installing rsync..."
yay -S rsync --noconfirm

mkdir $HOME/temp
cd $HOME/temp

print "Cloning .config repository..."
git clone https://gitlab.com/mdhedelund/config.git .config

print "Merging from $HOME/temp into $HOME/.config..."
rsync -arv $HOME/temp/.config $HOME/
sudo rm -rf $HOME/temp

### KEYBOARD
print "Installing custom us keyboard..."
sudo cp /usr/share/X11/xkb/symbols/us /usr/share/X11/xkb/symbols/us_backup
sudo cp $HOME/.config/keyboard_layout/us /usr/share/X11/xkb/symbols/us

### PACKAGES
print "Getting list of packages..."
file="$RUN_DIR/packages"
read -d $'\x04' packages < "$file"
echo $packages

print "Installing packages..."
yay -S $packages --noconfirm

### VS CODE EXTENSIONS
print "Getting list of VS Code extensions..."
file="$RUN_DIR/code_extensions"
read -d $'\x04' extensions < "$file"
echo $extensions

print "Installing VS Code extensions..."
for x in $extensions
do
    code --install-extension $x
done

### OH-MY-ZSH
print "Replacing ZSH variable in .config/.zshrc..."
sed -i "s|export ZSH=/home/mathias/.oh-my-zsh|export ZSH=/usr/share/oh-my-zsh|g" $HOME/.config/.zshrc

print "Symlinking .zshrc..."
ln -s $HOME/.config/.zshrc $HOME/.zshrc

### WALLPAPER
print "Symlinking wallpaper folder..."
ln -s ~/.config/Wallpaper ~/Pictures/Wallpaper

### CLEANUP
print "Removing unused dependencies..."
yay -Yc --noconfirm

### SHELL CHANGE
print "Changing shell from bash to zsh..."
chsh -s $(grep /zsh$ /etc/shells | tail -1)

### PRINT OS STATS
neofetch